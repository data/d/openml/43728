# OpenML dataset: 1996-2019-NBA-Stats-Complete-With-Player-Stats

https://www.openml.org/d/43728

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Naturally, I am myself an NBA fan and I don't prefer when I have data that has missing information. But not missing as like missing instance in columns, I mean the whole columns that I could use in my research or whatever I do. That is why I always scrape my data to ensure the quality of it. Here, I want to present to you one of the most complete NBA datasets somebody can get their hands on. 
This data represents the complete basic stats from every regular season game from 1996/1997 till the 2018/2019 season.
I already finished my business with this dataset, but I can't wait to see what are people from Kaggle going to do with it!
Credit: stats.nba.com

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43728) of an [OpenML dataset](https://www.openml.org/d/43728). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43728/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43728/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43728/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

